#pragma once

#include <misc/data_structures/Vector.h>
#include <variables/IntVariables.h>
#include <constraints/IntConstraints.h>

struct IntConstraintsPropagator
{
    IntVariables* variables;
    IntConstraints* constraints;

    Vector<bool> constraintToPropagate;


    bool someEmptyDomain;
    bool someConstraintsToPropagate;
    bool allConstraintsSatisfied;

    void initialize(IntVariables* variables, IntConstraints* constraints);
    void deinitialize();

    cudaDevice bool propagateConstraints();
    cudaDevice void manageDomainsEvents();
    cudaDevice void propagateRequiredConstraints();
    cudaDevice void updateDomains();
    cudaDevice void resetDomainsEvents();

    cudaDevice bool verifyConstraints();
    cudaDevice void updateSatisfiedConstraints();
};
