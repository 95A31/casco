#pragma once

#include <variables/IntVariables.h>
#include <constraints/IntConstraints.h>
#include <choosers/IntVariablesChooser.h>
#include <choosers/IntValuesChooser.h>
#include <propagators/IntConstraintsPropagator.h>
#include <searchers/IntBacktrackStack.h>

struct IntBacktrackSearcher
{
    enum Action
    {
        SetVariable,
        SetFirstValue,
        SetNextValue,
        PropagateConstraints
    };

    int action;
    int level;

    IntVariables* variables;
    IntConstraints* constraints;

    Vector<int> state;
    Vector<int> values;

    IntBacktrackStack stack;

    IntVariablesChooser variablesChooser;
    IntValuesChooser valuesChooser;

    IntConstraintsPropagator propagator;

    void initialize(IntVariables* variables, IntConstraints* constraints);
    void deinitialize();

    void setVariablesChooserType(int variableChooserType);
    void setValuesChooserType(int valueChooserType);

    cudaDevice void getFirstSolution(bool* solutionFound);
    cudaDevice void getNextSolution(bool* solutionFound);
};

