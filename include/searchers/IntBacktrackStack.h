#pragma once

#include <misc/data_structures/Vector.h>
#include <variables/IntVariables.h>
#include <domains/IntDomainsRepresentations.h>

struct IntBacktrackStack
{
    IntDomainsRepresentations* representations;

    Vector<IntDomainsRepresentations> stack;
    Vector<Vector<int>> stackInfo;

    void initialize(IntDomainsRepresentations* representations);
    void deinitialize();

    cudaDevice void saveState(int backtrackLevel);
    cudaDevice void restoreState(int backtrackLevel);
    cudaDevice void clearState(int backtrackLevel);

    cudaDevice bool isDomainChanged(int backtrackLevel, int variable);
};
