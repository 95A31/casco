#pragma once

#include <misc/data_structures/Vector.h>
#ifdef CUDA
#include <misc/data_structures/Lock.h>
#endif

struct IntDomainsActions
{
    Vector<Vector<int>> elementsToRemove;
    Vector<int> lowerbounds;
    Vector<int> upperbounds;
#ifdef CUDA
    Vector<Lock> locks;
#endif

    void initialize(int count);
    void deinitialize();

    void pushActions();

    cudaDevice void reset(int index);

    cudaDevice void removeElement(int index, int val);
    cudaDevice void removeAnyLesserThan(int index, int val);
    cudaDevice void removeAnyGreaterThan(int index, int val);
};
