#pragma once

#include <misc/data_structures/Vector.h>

struct IntDomainsRepresentations
{
    Vector<int> minimums;
    Vector<int> maximums;
    Vector<int> cardinalities;
    Vector<int> offsets;
    Vector<Vector<unsigned int>> bitvectors;

    void initialize(int count);
    cudaDevice void initializeBitvectors();
    void deinitialize();

    void pushRepresentation(int min, int max);
    cudaDevice void pushRepresentation(int min, int max, int offset, int cardinality, Vector<unsigned int>* bitvector);
    cudaDevice void popRepresentation();

    cudaDevice inline int getChunkIndex(int index, int val)
    {
        return abs(val - offsets[index]) / UINT_BIT_SIZE;
    }

    cudaDevice inline int getBitIndex(int index, int val)
    {
        return abs(val - offsets[index]) % UINT_BIT_SIZE;
    }

    cudaDevice  inline int getValue(int index, int chunkIndex, int bitIndex)
    {
        return offsets[index] + (UINT_BIT_SIZE * chunkIndex) + bitIndex;
    }

    cudaDevice bool contain(int index, int val);
    cudaDevice bool getNextValue(int index, int val, int* nextVal);
    cudaDevice bool getPrevValue(int index, int val, int* prevVal);

    cudaDevice void remove(int index, int val);
    cudaDevice void removeAnyLesserThan(int index, int val);
    cudaDevice void removeAnyGreaterThan(int index, int val);
    cudaDevice void removeAll(int index);
    cudaDevice bool keepOnly(int index, int val);

    cudaDevice int getIntervalCardinality(int index, int min, int max);
};
