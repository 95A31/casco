#pragma once

#include <domains/IntDomainsRepresentations.h>
#include <domains/IntDomainsActions.h>

struct IntDomains
{
    enum Event
    {
        None,
        Created,
        Changed,
        Empty
    };

    Vector<int> events;

    IntDomainsRepresentations representations;
    IntDomainsActions actions;

    void initialize(int count);
    void deinitialize();

    void pushDomain(int min, int max);

    cudaDevice void keepOnly(int index, int value);

    cudaDevice void performActions(int index);
};
