#pragma once

#include <misc/data_structures/Vector.h>
#include <variables/IntVariables.h>

#include <string>
#include <map>

struct IntConstraints
{

    static std::map<std::string, int> ContraintsIds;

    enum Type
    {
        IntLinNotEq,
        IntLinLe
    };

    int count;

    Vector<int> types;

    Vector<Vector<int>> variables;
    Vector<Vector<int>> parameters;

    void initialize();
    void deinitialize();

    void pushConstraint(int type);

    cudaDevice void propagate(int index, IntVariables* variables);
    cudaDevice bool satisfied(int index, IntVariables* variables);
};
