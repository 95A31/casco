#pragma once

#include <constraints/IntConstraints.h>

namespace IntLinNotEq
{
    cudaDevice void propagate(IntConstraints* constraints, int index, IntVariables* variables);
    cudaDevice bool satisfied(IntConstraints* constraints, int index, IntVariables* variables);
}
