#pragma once

#include <domains/IntDomains.h>
#include <misc/data_structures/Vector.h>

struct IntVariables
{
    int count;

    IntDomains domains;
    Vector<Vector<int>> constraints;

    void initialize(int count);
    void deinitialize();

    void pushVariable(int min, int max);

    cudaDevice inline bool isGround(int index)
    {
        return domains.representations.cardinalities[index] == 1;
    }

    cudaDevice void setGround(int index, int value);
};
