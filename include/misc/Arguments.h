#pragma once

#include <string>

#include <argp.h>

struct Arguments
{

    const char* solverVersion;
    long long int solutionsCount;
    char* flatzincFile;

    void initialize();
    int parseArguments(int argc, char * argv[]);
};

int parse_opt(int key, char *arg, struct argp_state *state);

