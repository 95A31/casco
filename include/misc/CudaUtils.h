#ifdef CUDA
#pragma once

#include <thrust/reduce.h>
#include <thrust/transform_reduce.h>
#include <thrust/execution_policy.h>

#include <misc/Utils.h>

#define WARP_SIZE 32
#define THREADS_PER_MULTIPROCESSOR 2048
#define BLOCK_PER_MULTIPROCESSOR 16
#define DEFAULT_BLOCK_SIZE (THREADS_PER_MULTIPROCESSOR / BLOCK_PER_MULTIPROCESSOR)

namespace KernelUtils
{
    cudaHostDevice inline int getBlockCount(int taskCount, int groupSize, bool divergence = true)
    {
        if (divergence)
        {
            return ceil(static_cast<float>(taskCount * WARP_SIZE) / groupSize);
        }
        else
        {
            return ceil(static_cast<float>(taskCount) / groupSize);
        }
    }

    cudaDevice inline int getTaskIndex(int threadIndex, bool divergence = true)
    {
        if (divergence)
        {
            if (threadIndex % WARP_SIZE != 0)
            {
                return -1;
            }
            else
            {
                return threadIndex / WARP_SIZE;
            }
        }
        else
        {
            return threadIndex;
        }
    }
}

namespace ParallelUtils
{

    template<typename BinaryFunction>
    cudaDevice void serialReduce(int* data, int dataCount, int* result, int paddingValue, BinaryFunction reduceFunction)
    {
        int value = paddingValue;

        for (int i = 0; i < dataCount; i += 1)
        {
            value = reduceFunction(value, data[i]);
        }

        *result = value;
    }

    template<typename BinaryFunction>
    cudaDevice inline void warpReduce(int* data, int dataCount, int* results, int paddingValue, BinaryFunction reduceFunction)
    {
        int localData = paddingValue;

        int threadId = (blockDim.x * blockIdx.x) + threadIdx.x;
        if (threadId < dataCount)
        {
            localData = data[threadId];
        }

        //Reduce warp data
        //Reference: https://devblogs.nvidia.com/parallelforall/faster-parallel-reductions-kepler/
        for (int threadOffset = WARP_SIZE / 2; threadOffset > 0; threadOffset /= 2)
        {
            localData = reduceFunction(localData, __shfl_down(localData, threadOffset, WARP_SIZE));
        }

        //Save reduce value
        if (threadIdx.x % WARP_SIZE == 0)
        {
            results[threadIdx.x / WARP_SIZE] = localData;
        }
    }

    template<typename BinaryFunction>
    cudaGlobal void blockReduce(int* data, int dataCount, int* results, int paddingValue, BinaryFunction reduceFunction)
    {
        __shared__ int buffer1[DEFAULT_BLOCK_SIZE / WARP_SIZE];
        __shared__ int buffer2[DEFAULT_BLOCK_SIZE / WARP_SIZE];

        int* currentBuffer = buffer1;
        int* nextBuffer = buffer2;

        warpReduce(data, dataCount, nextBuffer, paddingValue, reduceFunction);

        dataCount = DEFAULT_BLOCK_SIZE / WARP_SIZE;

        while (dataCount >= WARP_SIZE)
        {
            int* tmpBuffer = currentBuffer;
            currentBuffer = nextBuffer;
            nextBuffer = tmpBuffer;
            warpReduce(currentBuffer, dataCount, nextBuffer, paddingValue, reduceFunction);
            dataCount /= WARP_SIZE;
        }

        if (threadIdx.x == 0)
        {
            results[blockIdx.x] = nextBuffer[0];
        }
    }

    template<typename BinaryFunction>
    cudaDevice void gridReduce(int* data, int dataCount, int* result, int paddingValue, BinaryFunction reduceFunction)
    {
        int blockCount = ceil(static_cast<float>(dataCount) / DEFAULT_BLOCK_SIZE);

        int* currentBuffer;
        MemUtils::malloc(&currentBuffer, blockCount);

        int* nextBuffer;
        MemUtils::malloc(&nextBuffer, blockCount);

        blockReduce<<<blockCount, DEFAULT_BLOCK_SIZE>>>(data, dataCount, nextBuffer, paddingValue, reduceFunction);
        cudaDeviceSynchronize();

        dataCount = blockCount;
        blockCount = ceil(static_cast<float>(dataCount) / DEFAULT_BLOCK_SIZE);

        while (dataCount >= DEFAULT_BLOCK_SIZE)
        {
            int* tmpBuffer = currentBuffer;
            currentBuffer = nextBuffer;
            nextBuffer = tmpBuffer;
            blockReduce<<<blockCount, DEFAULT_BLOCK_SIZE>>>(currentBuffer, dataCount, nextBuffer, paddingValue, reduceFunction);
            cudaDeviceSynchronize();
            dataCount = blockCount;
            blockCount = ceil(static_cast<float>(dataCount) / DEFAULT_BLOCK_SIZE);
        }

        *result = nextBuffer[0];

        MemUtils::free(currentBuffer);
        MemUtils::free(nextBuffer);
    }

    template<typename BinaryFunction>
    cudaDevice void reduce(int* data, int dataCount, int* result, int paddingValue, BinaryFunction reduceFunction)
    {
        if (dataCount <= 135)
        {
            serialReduce(data, dataCount, result, paddingValue, reduceFunction);
        }
        else if (dataCount <= 1115000)
        {
            gridReduce(data, dataCount, result, paddingValue, reduceFunction);
        }
        else
        {
            *result = thrust::reduce(thrust::device, data, data + dataCount, paddingValue, reduceFunction);
        }
    }

    template<typename UnaryFunction, typename BinaryFunction>
    cudaDevice void serialTransformReduce(unsigned int* data, int dataCount, int* result, UnaryFunction transformFunction, int paddingValue,
            BinaryFunction reduceFunction)
    {
        int value = paddingValue;

        for (int i = 0; i < dataCount; i += 1)
        {
            value = reduceFunction(value, transformFunction(data[i]));
        }

        *result = value;
    }

    template<typename UnaryFunction, typename BinaryFunction>
    cudaDevice void warpTransformReduce(unsigned int* data, int dataCount, int* results, UnaryFunction transformFunction, int paddingValue,
            BinaryFunction reduceFunction)
    {
        int localData = paddingValue;

        int threadId = (blockDim.x * blockIdx.x) + threadIdx.x;
        if (threadId < dataCount)
        {
            localData = transformFunction(data[threadId]);
        }

        //Reduce warp data
        //Reference: https://devblogs.nvidia.com/parallelforall/faster-parallel-reductions-kepler/
        for (int threadOffset = WARP_SIZE / 2; threadOffset > 0; threadOffset /= 2)
        {
            localData = reduceFunction(localData, __shfl_down(localData, threadOffset, WARP_SIZE));
        }

        //Save reduce value
        if (threadIdx.x % WARP_SIZE == 0)
        {
            results[threadIdx.x / WARP_SIZE] = localData;
        }
    }

    template<typename UnaryFunction, typename BinaryFunction>
    cudaGlobal void blockTransformReduce(unsigned int* data, int dataCount, int* results, UnaryFunction transformFunction, int paddingValue,
            BinaryFunction reduceFunction)
    {
        __shared__ int buffer1[DEFAULT_BLOCK_SIZE / WARP_SIZE];
        __shared__ int buffer2[DEFAULT_BLOCK_SIZE / WARP_SIZE];

        int* currentBuffer = buffer1;
        int* nextBuffer = buffer2;

        warpTransformReduce(data, dataCount, nextBuffer, transformFunction, paddingValue, reduceFunction);

        dataCount = DEFAULT_BLOCK_SIZE / WARP_SIZE;

        while (dataCount >= WARP_SIZE)
        {
            int* tmpBuffer = currentBuffer;
            currentBuffer = nextBuffer;
            nextBuffer = tmpBuffer;
            warpReduce(currentBuffer, dataCount, nextBuffer, paddingValue, reduceFunction);
            dataCount /= WARP_SIZE;
        }

        if (threadIdx.x == 0)
        {
            results[blockIdx.x] = nextBuffer[0];
        }
    }

    template<typename UnaryFunction, typename BinaryFunction>
    cudaDevice void gridTransformReduce(unsigned int* data, int dataCount, int* result, UnaryFunction transformFunction, int paddingValue,
            BinaryFunction reduceFunction)
    {
        int blockCount = ceil(static_cast<float>(dataCount) / DEFAULT_BLOCK_SIZE);

        int* currentBuffer;
        MemUtils::malloc(&currentBuffer, blockCount);

        int* nextBuffer;
        MemUtils::malloc(&nextBuffer, blockCount);

        blockTransformReduce<<<blockCount, DEFAULT_BLOCK_SIZE>>>(data, dataCount, nextBuffer, transformFunction, paddingValue, reduceFunction);
        cudaDeviceSynchronize();

        dataCount = blockCount;
        blockCount = ceil(static_cast<float>(dataCount) / DEFAULT_BLOCK_SIZE);

        while (dataCount >= DEFAULT_BLOCK_SIZE)
        {
            int* tmpBuffer = currentBuffer;
            currentBuffer = nextBuffer;
            nextBuffer = tmpBuffer;
            blockReduce<<<blockCount, DEFAULT_BLOCK_SIZE>>>(currentBuffer, dataCount, nextBuffer, paddingValue, reduceFunction);
            cudaDeviceSynchronize();
            dataCount = blockCount;
            blockCount = ceil(static_cast<float>(dataCount) / DEFAULT_BLOCK_SIZE);
        }

        *result = nextBuffer[0];

        MemUtils::free(currentBuffer);
        MemUtils::free(nextBuffer);
    }

    template<typename UnaryFunction, typename BinaryFunction>
    cudaDevice void transformReduce(unsigned int* data, int dataCount, int* result, UnaryFunction transformFunction, int paddingValue,
            BinaryFunction reduceFunction)
    {
        if (dataCount <= 85)
        {
            serialTransformReduce(data, dataCount, result, transformFunction, paddingValue, reduceFunction);
        }
        else if (dataCount <= 3055000)
        {
            gridTransformReduce(data, dataCount, result, transformFunction, paddingValue, reduceFunction);
        }
        else
        {
            *result = thrust::transform_reduce(thrust::device, data, data + dataCount, transformFunction, paddingValue, reduceFunction);
        }
    }

    template<typename T>
    cudaDevice void serialFill(T* data, int dataCount, T value)
    {
        for (int i = 0; i < dataCount; i += 1)
        {
            data[i] = value;
        }
    }

    template<typename T>
    cudaGlobal void blockFill(T* data, int dataCount, T value)
    {
        int i = (blockDim.x * blockIdx.x) + threadIdx.x;
        if (i >= 0 and i < dataCount)
        {
            data[i] = value;
        }
    }

    template<typename T>
    cudaDevice void fill(T* data, int dataCount, T value)
    {
        if (dataCount <= 135)
        {
            serialFill(data, dataCount, value);
        }
        else
        {
            int blockCount = ceil(static_cast<float>(dataCount) / DEFAULT_BLOCK_SIZE);
            blockFill<<<blockCount, DEFAULT_BLOCK_SIZE>>>(data, dataCount, value);
            cudaDeviceSynchronize();
        }
    }
}
#endif
