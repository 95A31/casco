#pragma once

#include <cassert>

#include <misc/Utils.h>
#ifdef CUDA
#include <misc/CudaUtils.h>
#endif

#define VECTOR_INITIAL_CAPACITY 32

template<typename T>
struct Vector
{
    int size;
    int capacity;
    T* data;

    cudaHostDevice void initialize(int initialCapacity = VECTOR_INITIAL_CAPACITY)
    {
        size = 0;
        capacity = initialCapacity;
        MemUtils::malloc<T>(&data, initialCapacity);
    }

    cudaHostDevice void initialize(Vector<T>* other)
    {
        initialize(other->size);
        copy(other);
    }

    cudaHostDevice void deinitialize()
    {
        MemUtils::free<T>(data);
    }

    cudaHostDevice inline T& operator[](int index)
    {
        assert(index < size);

        return data[index];
    }

    cudaHostDevice const inline T& operator[](int index) const
    {
        assert(index < size);

        return data[index];
    }

    cudaHostDevice inline T& at(int index)
    {
        assert(index < size);

        return data[index];
    }

    cudaHostDevice const inline T& at(int index) const
    {
        assert(index < size);

        return data[index];
    }

    cudaHostDevice void copy(Vector<T>* other)
    {
        if (capacity < other->size)
        {
            MemUtils::realloc<T>(&data, other->size, capacity);
            capacity = other->size;
        }

        MemUtils::memcpy<T>(data, other->data, other->size);
        size = other->size;
    }

    cudaHostDevice void resize(int count)
    {
        if (capacity < count)
        {
            MemUtils::realloc<T>(&data, count * 2, capacity);
            capacity = count * 2;
        }

        size = count;
    }

    cudaHostDevice void resize_by_one()
    {
        resize(size + 1);
    }

    cudaHostDevice inline T& back()
    {
        assert(size > 0);

        return data[size - 1];
    }

    cudaHostDevice void push_back(T t)
    {
        if (size == capacity)
        {
            MemUtils::realloc<T>(&data, capacity * 2, capacity);
            capacity = capacity * 2;
        }

        data[size] = t;
        size += 1;
    }

    cudaHostDevice inline void pop_back()
    {
        assert(size > 0);

        size -= 1;
    }

    cudaHostDevice void fillWith(int startIndex, int endIndex, T value)
    {
        assert(startIndex >= 0 and endIndex <= size);

#if defined(__CUDA_ARCH__) && defined(CUDA)
        ParallelUtils::fill(data + startIndex, endIndex - startIndex, value);
#else
        for (int i = startIndex; i < endIndex; i += 1)
        {
            data[i] = value;
        }
#endif
    }

    cudaHostDevice void fillWith(T value)
    {
        fillWith(0, size, value);
    }

    cudaHostDevice void clear()
    {
        size = 0;
    }
};
