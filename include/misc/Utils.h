#pragma once

#include <cstring>
#include <cassert>

#define UINT_BIT_SIZE 32

#ifdef CUDA
#define cudaGlobal __global__
#define cudaDevice __device__
#define cudaHostDevice __host__ __device__
#else
#define cudaGlobal
#define cudaDevice
#define cudaHostDevice
#endif

namespace LogUtils
{
    cudaHostDevice void error(const char* function, const char* msg);
#ifdef CUDA
    void cudaAssert(const char* function, cudaError_t code);
#endif
}

namespace MemUtils
{
    template<typename T>
    cudaHostDevice void malloc(T** ptr, int count = 1)
    {
#if defined(__CUDA_ARCH__) || !defined(CUDA)
        *ptr = static_cast<T*>(std::malloc(sizeof(T) * count));
#else
        LogUtils::cudaAssert(__PRETTY_FUNCTION__, cudaMallocManaged(ptr, sizeof(T) * count));
#endif
        assert(*ptr != nullptr);
    }

    template<typename T>
    cudaHostDevice void free(T* ptr)
    {
#if defined(__CUDA_ARCH__) || !defined(CUDA)
        std::free(ptr);
#else
        LogUtils::cudaAssert(__PRETTY_FUNCTION__, cudaFree(ptr));
#endif
    }

    template<typename T>
    cudaHostDevice void memcpy(T* dst, T* src, int count)
    {
        std::memcpy(dst, src, sizeof(T) * count);
    }

    template<typename T>
    cudaHostDevice void realloc(T** ptr, int newCount, int oldCount)
    {
#ifdef CUDA
        T* ptrTmp;
        MemUtils::malloc<T>(&ptrTmp, newCount);
        MemUtils::memcpy<T>(ptrTmp, *ptr, oldCount);
        MemUtils::free<T>(*ptr);
        *ptr = ptrTmp;
#else
        *ptr = static_cast<T*>(std::realloc(*ptr, sizeof(T) * newCount));
#endif
    }
}

namespace BitsUtils
{
    cudaDevice inline int getLeftmostOneIndex(unsigned int val)
    {
#ifdef __CUDA_ARCH__
        return __clz(val);
#else
        return __builtin_clz(val);
#endif
    }

    cudaDevice inline int getRightmostOneIndex(unsigned int val)
    {
#ifdef __CUDA_ARCH__
        return UINT_BIT_SIZE - __ffs(val);
#else
        return UINT_BIT_SIZE - __builtin_ffs(val);
#endif
    }

    cudaDevice inline int getOnesCount(unsigned int val)
    {
#ifdef __CUDA_ARCH__
        return __popc(val);
#else
        return __builtin_popcount(val);
#endif
    }

    cudaDevice inline unsigned int getMask(int bitIndex)
    {
        return 1 << UINT_BIT_SIZE - 1 - bitIndex;
    }

    cudaDevice inline unsigned int getLeftFilledMask(int bitIndex)
    {
        return UINT_MAX << UINT_BIT_SIZE - 1 - bitIndex;
    }

    cudaDevice inline unsigned int getRightFilledMask(int bitIndex)
    {
        return UINT_MAX >> bitIndex;
    }
}
