#ifdef CUDA
#pragma once

#include <searchers/IntBacktrackSearcher.h>

namespace Wrappers
{
    //Integer backtrack searcher
    cudaGlobal void getFirstSolution(IntBacktrackSearcher* backtrackSearcher, bool* solutionFound);
    cudaGlobal void getNextSolution(IntBacktrackSearcher* backtrackSearcher, bool* solutionFound);

    //Integer backtrack stack
    cudaGlobal void saveState(IntBacktrackStack* backtrackStack, int backtrackLevel);
    cudaGlobal void restoreState(IntBacktrackStack* backtrackStack, int backtrackLevel);
    cudaGlobal void clearState(IntBacktrackStack* backtrackStack, int backtrackLevel);

    //Integer constraints propagator
    cudaGlobal void propagateRequiredConstraints(IntConstraintsPropagator* propagator);
    cudaGlobal void updateDomains(IntConstraintsPropagator* propagator);
    cudaGlobal void manageDomainsEvents(IntConstraintsPropagator* propagator);
    cudaGlobal void updateSatisfiedConstraints(IntConstraintsPropagator* propagator);

    //Bitvectors initialization
    cudaGlobal void initializeBitvectors(IntDomainsRepresentations* representations);
}
#endif
