# CASCO
CASCO (Cuda Accelerated Simple Constraint sOlver) is a constraint solver that exploit GPU computational power in order to solve constraints problems. It is designed with simplicity and performances as primary goals. 

## Requirements
- Nvidia GPU with [compute capability](https://en.wikipedia.org/wiki/CUDA#GPUs_supported) >= 3.5 
- CMake >= 3.5.1
- GCC >= 5.4
- CUDA SDK >= 7.5

## Compilation
Execute the following commands:
```sh
mkdir build
cd build
cmake ..
make
```
