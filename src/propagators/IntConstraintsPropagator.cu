#include <propagators/IntConstraintsPropagator.h>
#include <misc/Utils.h>
#include <misc/Wrappers.h>
#include <misc/CudaUtils.h>

void IntConstraintsPropagator::initialize(IntVariables* variables, IntConstraints* constraints)
{
    this->variables = variables;
    this->constraints = constraints;

    constraintToPropagate.initialize(constraints->count);
    constraintToPropagate.resize(constraints->count);
}

void IntConstraintsPropagator::deinitialize()
{
    constraintToPropagate.deinitialize();
}

cudaDevice bool IntConstraintsPropagator::propagateConstraints()
{
    someEmptyDomain = false;
    someConstraintsToPropagate = false;
#ifdef CUDA
    int constraintsBlockCountDivergence = KernelUtils::getBlockCount(constraints->count, DEFAULT_BLOCK_SIZE);
    int constraintsBlockCount = KernelUtils::getBlockCount(constraints->count, DEFAULT_BLOCK_SIZE, false);
    int variablesBlockCount = KernelUtils::getBlockCount(variables->count, DEFAULT_BLOCK_SIZE, false);

    Wrappers::manageDomainsEvents<<<constraintsBlockCount, DEFAULT_BLOCK_SIZE>>>(this);
    cudaDeviceSynchronize();
#else
    manageDomainsEvents();
#endif

    while (someConstraintsToPropagate and (not someEmptyDomain))
    {
#ifdef CUDA
        Wrappers::propagateRequiredConstraints<<<constraintsBlockCountDivergence, DEFAULT_BLOCK_SIZE>>>(this);
        cudaDeviceSynchronize();
#else
        propagateRequiredConstraints();
#endif

#ifdef CUDA
        Wrappers::updateDomains<<<variablesBlockCount, DEFAULT_BLOCK_SIZE>>>(this);
        cudaDeviceSynchronize();
#else
        updateDomains();
#endif

        someEmptyDomain = false;
        someConstraintsToPropagate = false;
#ifdef CUDA
        Wrappers::manageDomainsEvents<<<constraintsBlockCount, DEFAULT_BLOCK_SIZE>>>(this);
        cudaDeviceSynchronize();
#else
        manageDomainsEvents();
#endif

        resetDomainsEvents();
    }

    return (not someEmptyDomain);
}

cudaDevice void IntConstraintsPropagator::propagateRequiredConstraints()
{
#ifdef CUDA
    int i = KernelUtils::getTaskIndex(blockDim.x * blockIdx.x + threadIdx.x);
    if (i >= 0 and i < constraints->count)
#else
    for (int i = 0; i < constraints->count; i += 1)
#endif
    {
        if (constraintToPropagate[i])
        {
            constraints->propagate(i, variables);
            constraintToPropagate[i] = false;
        }
    }
}

cudaDevice void IntConstraintsPropagator::updateDomains()
{
#ifdef CUDA
    int i = KernelUtils::getTaskIndex(blockDim.x * blockIdx.x + threadIdx.x, false);
    if (i >= 0 and i < variables->count)
#else
    for (int i = 0; i < variables->count; i += 1)
#endif
    {
        variables->domains.performActions(i);
    }
}

cudaDevice void IntConstraintsPropagator::manageDomainsEvents()
{
#ifdef CUDA
    int ci = KernelUtils::getTaskIndex(blockDim.x * blockIdx.x + threadIdx.x, false);
    if (ci >= 0 and ci < constraints->count)
#else
    for (int ci = 0; ci < constraints->count; ci += 1)
#endif
    {
        for (int vi = 0; vi < constraints->variables[ci].size; vi += 1)
        {
            int event = variables->domains.events[constraints->variables[ci][vi]];

            if (event == IntDomains::Event::Empty)
            {
                someEmptyDomain = true;
            }

            if (event == IntDomains::Event::Created or event == IntDomains::Event::Changed)
            {
                constraintToPropagate[ci] = true;
                someConstraintsToPropagate = true;
            }
        }
    }
}

cudaDevice void IntConstraintsPropagator::resetDomainsEvents()
{
    variables->domains.events.fillWith(IntDomains::Event::None);
}

cudaDevice bool IntConstraintsPropagator::verifyConstraints()
{
    allConstraintsSatisfied = true;
#ifdef CUDA
    int constraintsBlockCountDivergence = KernelUtils::getBlockCount(constraints->count, DEFAULT_BLOCK_SIZE);

    Wrappers::updateSatisfiedConstraints<<<constraintsBlockCountDivergence, DEFAULT_BLOCK_SIZE>>>(this);
    cudaDeviceSynchronize();
#else
    updateSatisfiedConstraints();
#endif

    return allConstraintsSatisfied;
}

cudaDevice void IntConstraintsPropagator::updateSatisfiedConstraints()
{
#ifdef CUDA
    int i = KernelUtils::getTaskIndex(blockDim.x * blockIdx.x + threadIdx.x);
    if (i >= 0 and i < constraints->count)
#else
    for (int i = 0; i < constraints->count; i += 1)
#endif
    {
        if (not constraints->satisfied(i, variables))
        {
            allConstraintsSatisfied = false;
        }
    }
}
