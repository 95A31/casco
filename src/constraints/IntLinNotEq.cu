#include <constraints/IntLinNotEq.h>

cudaDevice void IntLinNotEq::propagate(IntConstraints* constraints, int index, IntVariables* variables)
{
    //Constraint propagation is performed when only one variable is not ground

    Vector<int>* constraintVariables = &constraints->variables[index];
    Vector<int>* constraintParameters = &constraints->parameters[index];

    int notGroundVariablesCount = 0;
    int notGroundVariableIndex = 0;
    for (int i = 0; i < constraintVariables->size; i += 1)
    {
        if (not variables->isGround(constraintVariables->at(i)))
        {
            notGroundVariablesCount += 1;
            notGroundVariableIndex = i;
        }
    }

    if (notGroundVariablesCount == 1)
    {
        int value = 0;
        for (int i = 0; i < constraintVariables->size; i += 1)
        {
            if (i != notGroundVariableIndex)
            {
                value += constraintParameters->at(i) * variables->domains.representations.minimums[constraintVariables->at(i)];
            }
        }
        value = -value + constraintParameters->back();

        if (value % constraintParameters->at(notGroundVariableIndex) == 0)
        {
            value /= constraintParameters->at(notGroundVariableIndex);
            variables->domains.actions.removeElement(constraintVariables->at(notGroundVariableIndex), value);
        }
    }
}

cudaDevice bool IntLinNotEq::satisfied(IntConstraints* constraints, int index, IntVariables* variables)
{
    //Satisfaction check is performed only when all variables is ground

    Vector<int>* constraintVariables = &constraints->variables[index];
    Vector<int>* constraintParameters = &constraints->parameters[index];

    for (int i = 0; i < constraintVariables->size; i += 1)
    {
        if (not variables->isGround(constraintVariables->at(i)))
        {
            return true;
        }
    }

    int value = 0;
    for (int i = 0; i < constraintVariables->size; i += 1)
    {
        value += constraintParameters->at(i) * variables->domains.representations.minimums[constraintVariables->at(i)];
    }

    return value != constraintParameters->back();
}
