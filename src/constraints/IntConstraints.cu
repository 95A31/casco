#include <constraints/IntConstraints.h>
#include <misc/Utils.h>
#include <constraints/IntLinNotEq.h>
#include <constraints/IntLinLe.h>

std::map<std::string, int> IntConstraints::ContraintsIds =
    {
        { "int_lin_ne", IntConstraints::Type::IntLinNotEq },
        { "int_lin_le", IntConstraints::Type::IntLinLe }
    };

void IntConstraints::initialize()
{
    count = 0;

    types.initialize();

    variables.initialize();
    parameters.initialize();
}

void IntConstraints::pushConstraint(int type)
{
    types.push_back(type);

    variables.resize_by_one();
    variables.back().initialize();

    parameters.resize_by_one();
    parameters.back().initialize();

    count += 1;
}

void IntConstraints::deinitialize()
{
    types.deinitialize();

    variables.deinitialize();
    parameters.deinitialize();
}

cudaDevice void IntConstraints::propagate(int index, IntVariables* variables)
{
    switch (types[index])
    {
        case IntLinNotEq:
            IntLinNotEq::propagate(this, index, variables);
            break;
        case IntLinLe:
            IntLinLe::propagate(this, index, variables);
            break;
        default:
            LogUtils::error(__PRETTY_FUNCTION__, "Invalid constraint type");
    }
}

cudaDevice bool IntConstraints::satisfied(int index, IntVariables* variables)
{
    switch (types[index])
    {
        case IntLinNotEq:
            return IntLinNotEq::satisfied(this, index, variables);
        case IntLinLe:
            return IntLinLe::satisfied(this, index, variables);
        default:
            LogUtils::error(__PRETTY_FUNCTION__, "Invalid constraint type");
            return false;
    }
}
