#include <constraints/IntLinLe.h>

cudaDevice void IntLinLe::propagate(IntConstraints* constraints, int index, IntVariables* variables)
{
    Vector<int>* constraintVariables = &constraints->variables[index];
    Vector<int>* constraintParameters = &constraints->parameters[index];

    int minSum = 0;
    for (int i = 0; i < constraintVariables->size; i += 1)
    {
        int variableCoefficient = constraintParameters->at(i);
        int variableValue = 0;

        if (variableCoefficient > 0)
        {
            variableValue = variables->domains.representations.minimums[constraintVariables->at(i)];
        }
        else
        {
            variableValue = variables->domains.representations.maximums[constraintVariables->at(i)];
        }
        minSum += variableCoefficient * variableValue;
    }

    int c = constraintParameters->back();
    for (int i = 0; i < constraintVariables->size; i += 1)
    {
        int variable = constraintVariables->at(i);
        int variableContribution = 0;
        int variableCoefficient = constraintParameters->at(i);

        if (variableCoefficient > 0)
        {
            variableContribution = variableCoefficient * variables->domains.representations.minimums[variable];
            int upperbound = (c - (minSum - variableContribution)) / variableCoefficient;
            variables->domains.actions.removeAnyGreaterThan(variable, upperbound);
        }
        else
        {
            variableContribution = variableCoefficient * variables->domains.representations.maximums[variable];
            int lowerbound = (c - (minSum - variableContribution)) / variableCoefficient;
            variables->domains.actions.removeAnyLesserThan(variable, lowerbound);
        }
    }
}

cudaDevice bool IntLinLe::satisfied(IntConstraints* constraints, int index, IntVariables* variables)
{
    Vector<int>* constraintVariables = &constraints->variables[index];
    Vector<int>* constraintParameters = &constraints->parameters[index];

    //Satisfaction check is performed only when all variables is ground
    for (int i = 0; i < constraintVariables->size; i += 1)
    {
        if (not variables->isGround(constraintVariables->at(i)))
        {
            return true;
        }
    }

    int sum = 0;
    for (int i = 0; i < constraintVariables->size; i += 1)
    {
        sum += constraintParameters->at(i) * variables->domains.representations.minimums[constraintVariables->at(i)];
    }

    return sum <= constraintParameters->back();
}
