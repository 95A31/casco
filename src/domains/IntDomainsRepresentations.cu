#include <thrust/functional.h>

#include <domains/IntDomainsRepresentations.h>
#include <misc/Utils.h>
#include <misc/CudaUtils.h>

void IntDomainsRepresentations::initialize(int count)
{
    minimums.initialize(count);
    maximums.initialize(count);
    cardinalities.initialize(count);
    offsets.initialize(count);
    bitvectors.initialize(count);
}

cudaDevice void IntDomainsRepresentations::initializeBitvectors()
{
#ifdef CUDA
    int i = KernelUtils::getTaskIndex(blockDim.x * blockIdx.x + threadIdx.x, false);
    if (i >= 0 and i < bitvectors.size)
#else
    for (int i = 0; i < bitvectors.size; i += 1)
#endif
    {
        int maxChunkIndex = getChunkIndex(i, maximums[i]);
        int chunksCount = maxChunkIndex + 1;
        bitvectors[i].initialize(chunksCount);
        bitvectors[i].resize(chunksCount);
        bitvectors[i].fillWith(UINT_MAX);
        int maxBitIndex = getBitIndex(i, maximums[i]);
        unsigned int mask = BitsUtils::getLeftFilledMask(maxBitIndex);
        bitvectors[i][maxChunkIndex] &= mask;
    }
}

void IntDomainsRepresentations::deinitialize()
{
    minimums.deinitialize();
    maximums.deinitialize();
    cardinalities.deinitialize();
    offsets.deinitialize();

    for (int i = 0; i < bitvectors.size; i += 1)
    {
        bitvectors[i].deinitialize();
    }
    bitvectors.initialize();
}

void IntDomainsRepresentations::pushRepresentation(int min, int max)
{
    minimums.push_back(min);
    maximums.push_back(max);
    cardinalities.push_back(abs(max - min) + 1);
    offsets.push_back(min);
    bitvectors.resize_by_one();
}

cudaDevice void IntDomainsRepresentations::pushRepresentation(int min, int max, int offset, int cardinality, Vector<unsigned int>* bitvector)
{
    minimums.push_back(min);
    maximums.push_back(max);
    cardinalities.push_back(cardinality);
    offsets.push_back(offset);
    bitvectors.resize_by_one();
    bitvectors.back().initialize(bitvector);
}

cudaDevice void IntDomainsRepresentations::popRepresentation()
{
    minimums.pop_back();
    maximums.pop_back();
    cardinalities.pop_back();
    offsets.pop_back();
    bitvectors.back().deinitialize();
    bitvectors.pop_back();
}

cudaDevice bool IntDomainsRepresentations::contain(int index, int val)
{
    if (minimums[index] <= val and val <= maximums[index])
    {
        int valChunkIndex = getChunkIndex(index, val);
        int valBitIndex = getBitIndex(index, val);

        unsigned int mask = BitsUtils::getMask(valBitIndex);
        return (bitvectors[index][valChunkIndex] & mask) != 0;
    }
    else
    {
        return false;
    }
}

cudaDevice bool IntDomainsRepresentations::getNextValue(int index, int val, int* nextVal)
{
    if (val < maximums[index])
    {
        int nextValChunkIndex = getChunkIndex(index, val);
        int valBitIndex = getBitIndex(index, val);

        unsigned int mask = ~BitsUtils::getLeftFilledMask(valBitIndex);
        unsigned int nextValChunk = bitvectors[index][nextValChunkIndex] & mask;

        if (nextValChunk == 0)
        {
            int maxChunkIndex = getChunkIndex(index, maximums[index]);
            for (nextValChunkIndex = nextValChunkIndex + 1; nextValChunkIndex <= maxChunkIndex; nextValChunkIndex += 1)
            {
                if (bitvectors[index][nextValChunkIndex] != 0)
                {
                    break;
                }
            }

            nextValChunk = bitvectors[index][nextValChunkIndex];
        }

        *nextVal = getValue(index, nextValChunkIndex, BitsUtils::getLeftmostOneIndex(nextValChunk));
        return true;
    }
    else
    {
        return false;
    }
}

cudaDevice bool IntDomainsRepresentations::getPrevValue(int index, int val, int* prevVal)
{
    if (minimums[index] < val)
    {
        int prevValChunkIndex = getChunkIndex(index, val);
        int valBitIndex = getBitIndex(index, val);

        unsigned int mask = ~BitsUtils::getRightFilledMask(valBitIndex);
        unsigned int prevValChunk = bitvectors[index][prevValChunkIndex] & mask;

        if (prevValChunk == 0)
        {
            int minChunkIndex = getChunkIndex(index, minimums[index]);
            for (prevValChunkIndex = prevValChunkIndex - 1; prevValChunkIndex >= minChunkIndex; prevValChunkIndex -= 1)
            {
                if (bitvectors[index][prevValChunkIndex] != 0)
                {
                    break;
                }
            }

            prevValChunk = bitvectors[index][prevValChunkIndex];
        }

        *prevVal = getValue(index, prevValChunkIndex, BitsUtils::getRightmostOneIndex(prevValChunk));
        return true;
    }
    else
    {
        return false;
    }
}

cudaDevice void IntDomainsRepresentations::remove(int index, int val)
{
    if (contain(index, val))
    {
        int valChunkIndex = getChunkIndex(index, val);
        int valBitIndex = getBitIndex(index, val);

        unsigned int mask = ~BitsUtils::getMask(valBitIndex);

        bitvectors[index][valChunkIndex] &= mask;

        cardinalities[index] -= 1;

        if (cardinalities[index] > 0)
        {
            if (val == minimums[index])
            {
                getNextValue(index, val, &minimums[index]);
            }

            if (val == maximums[index])
            {
                getPrevValue(index, val, &maximums[index]);
            }
        }
        else
        {
            minimums[index] = 0;
            maximums[index] = 0;
        }
    }
}

cudaDevice void IntDomainsRepresentations::removeAnyLesserThan(int index, int val)
{
    if (minimums[index] < val and val <= maximums[index])
    {
        int newMinimum = minimums[index];

        if (contain(index, val))
        {
            newMinimum = val;
        }
        else
        {
            getNextValue(index, val, &newMinimum);
        }

        if ((newMinimum - minimums[index]) >= (maximums[index] - newMinimum))
        {
            cardinalities[index] = getIntervalCardinality(index, newMinimum, maximums[index]);
        }
        else
        {
            cardinalities[index] -= getIntervalCardinality(index, minimums[index], newMinimum - 1);
        }

        minimums[index] = newMinimum;
    }
    else if (val > maximums[index])
    {
        removeAll(index);
    }
}

cudaDevice void IntDomainsRepresentations::removeAnyGreaterThan(int index, int val)
{
    if (minimums[index] <= val and val < maximums[index])
    {
        int newMaximum = maximums[index];

        if (contain(index, val))
        {
            newMaximum = val;
        }
        else
        {
            getPrevValue(index, val, &newMaximum);
        }

        if ((newMaximum - minimums[index]) >= (maximums[index] - newMaximum))
        {
            cardinalities[index] -= getIntervalCardinality(index, newMaximum + 1, maximums[index]);
        }
        else
        {
            cardinalities[index] = getIntervalCardinality(index, minimums[index], newMaximum);
        }

        maximums[index] = newMaximum;
    }
    else if (val < minimums[index])
    {
        removeAll(index);
    }
}

cudaDevice bool IntDomainsRepresentations::keepOnly(int index, int val)
{
    if (contain(index, val))
    {
        minimums[index] = val;
        maximums[index] = val;
        cardinalities[index] = 1;
        return true;
    }
    else
    {
        removeAll(index);
        return false;
    }
}

cudaDevice void IntDomainsRepresentations::removeAll(int index)
{
    minimums[index] = 0;
    maximums[index] = 0;
    cardinalities[index] = 0;
}

cudaDevice int IntDomainsRepresentations::getIntervalCardinality(int index, int min, int max)
{
    int minChunkIndex = getChunkIndex(index, min);
    int minBitIndex = getBitIndex(index, min);
    unsigned int minChunk = bitvectors[index][minChunkIndex] & BitsUtils::getRightFilledMask(minBitIndex);

    int maxChunkIndex = getChunkIndex(index, max);
    int maxBitIndex = getBitIndex(index, max);
    unsigned int maxChunk = bitvectors[index][maxChunkIndex] & BitsUtils::getLeftFilledMask(maxBitIndex);

    if (minChunkIndex == maxChunkIndex)
    {
        return BitsUtils::getOnesCount(minChunk & maxChunk);
    }

    int cardinality = BitsUtils::getOnesCount(minChunk) + BitsUtils::getOnesCount(maxChunk);
    if (minChunkIndex == maxChunkIndex - 1)
    {
        return cardinality;
    }

#ifdef CUDA
    int centerCardinality = 0;
    ParallelUtils::transformReduce(bitvectors[index].data + minChunkIndex + 1, maxChunkIndex - minChunkIndex, &centerCardinality, BitsUtils::getOnesCount, 0,
            thrust::plus<int>());

    cardinality += centerCardinality;
#else
    for (int ci = minChunkIndex + 1; ci < maxChunkIndex; ci += 1)
    {
        cardinality += BitsUtils::getOnesCount(bitvectors[index][ci]);
    }
#endif

    return cardinality;
}
