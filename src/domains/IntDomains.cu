#include <domains/IntDomains.h>

void IntDomains::initialize(int count)
{
    events.initialize(count);

    representations.initialize(count);
    actions.initialize(count);
}

void IntDomains::deinitialize()
{
    events.deinitialize();

    representations.deinitialize();
    actions.deinitialize();
}

void IntDomains::pushDomain(int min, int max)
{
    events.push_back(Event::Created);

    representations.pushRepresentation(min, max);
    actions.pushActions();
}

cudaDevice void IntDomains::keepOnly(int index, int value)
{
    int previousCardinality = representations.cardinalities[index];

    representations.keepOnly(index, value);

    if (representations.cardinalities[index] < previousCardinality)
    {
        events[index] = Event::Changed;
    }
}

cudaDevice void IntDomains::performActions(int index)
{
    int previousCardinality = representations.cardinalities[index];

    representations.removeAnyGreaterThan(index, actions.upperbounds[index]);

    representations.removeAnyLesserThan(index, actions.lowerbounds[index]);

    for (int i = 0; i < actions.elementsToRemove[index].size; i += 1)
    {
        representations.remove(index, actions.elementsToRemove[index][i]);
    }

    actions.reset(index);

    if (representations.cardinalities[index] == 0)
    {
        events[index] = Event::Empty;
    }
    else if (representations.cardinalities[index] < previousCardinality)
    {
        events[index] = Event::Changed;
    }
    else
    {
        events[index] = Event::None;
    }

}
