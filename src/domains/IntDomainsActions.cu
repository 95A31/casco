#include <domains/IntDomainsActions.h>
#include <misc/Utils.h>

void IntDomainsActions::initialize(int count)
{
    elementsToRemove.initialize(count);

    lowerbounds.initialize(count);
    upperbounds.initialize(count);

#ifdef CUDA
    locks.initialize(count);
#endif
}

void IntDomainsActions::deinitialize()
{
    for (int i = 0; i < elementsToRemove.size; i += 1)
    {
        elementsToRemove[i].deinitialize();
    }
    elementsToRemove.deinitialize();

    lowerbounds.deinitialize();
    upperbounds.deinitialize();

#ifdef CUDA
    locks.deinitialize();
#endif
}

void IntDomainsActions::pushActions()
{
    elementsToRemove.resize_by_one();
    elementsToRemove.back().initialize();

    lowerbounds.push_back(INT_MIN);
    upperbounds.push_back(INT_MAX);

#ifdef CUDA
    locks.resize_by_one();
    locks.back().initialize();
#endif
}

cudaDevice void IntDomainsActions::reset(int index)
{
    elementsToRemove[index].clear();

    lowerbounds[index] = INT_MIN;
    upperbounds[index] = INT_MAX;
}

cudaDevice void IntDomainsActions::removeElement(int index, int val)
{
    if (lowerbounds[index] <= val and val <= upperbounds[index])
    {
#ifdef CUDA
        locks[index].lock();
#endif
        elementsToRemove[index].push_back(val);
#ifdef CUDA
        locks[index].unlock();
#endif
    }
}

cudaDevice void IntDomainsActions::removeAnyGreaterThan(int index, int val)
{
#ifdef CUDA
    __threadfence();
    atomicMin(&upperbounds[index], val);
#else
    upperbounds[index] = min(val, upperbounds[index]);
#endif
}

cudaDevice void IntDomainsActions::removeAnyLesserThan(int index, int val)
{
#ifdef CUDA
    __threadfence();
    atomicMax(&lowerbounds[index], val);
#else
    lowerbounds[index] = max(val, lowerbounds[index]);
#endif
}
