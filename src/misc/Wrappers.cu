#ifdef CUDA
#include <misc/Wrappers.h>

cudaGlobal void Wrappers::getFirstSolution(IntBacktrackSearcher* backtrackSearcher, bool* solutionFound)
{
    backtrackSearcher->getFirstSolution(solutionFound);
}

cudaGlobal void Wrappers::getNextSolution(IntBacktrackSearcher* backtrackSearcher, bool* solutionFound)
{
    backtrackSearcher->getNextSolution(solutionFound);
}

cudaGlobal void Wrappers::saveState(IntBacktrackStack* backtrackStack, int backtrackLevel)
{
    backtrackStack->saveState(backtrackLevel);
}

cudaGlobal void Wrappers::restoreState(IntBacktrackStack* backtrackStack, int backtrackLevel)
{
    backtrackStack->restoreState(backtrackLevel);
}

cudaGlobal void Wrappers::clearState(IntBacktrackStack* backtrackStack, int backtrackLevel)
{
    backtrackStack->clearState(backtrackLevel);
}

cudaGlobal void Wrappers::propagateRequiredConstraints(IntConstraintsPropagator* propagator)
{
    propagator->propagateRequiredConstraints();
}

cudaGlobal void Wrappers::updateDomains(IntConstraintsPropagator* propagator)
{
    propagator->updateDomains();
}

cudaGlobal void Wrappers::manageDomainsEvents(IntConstraintsPropagator* propagator)
{
    propagator->manageDomainsEvents();
}

cudaGlobal void Wrappers::updateSatisfiedConstraints(IntConstraintsPropagator* propagator)
{
    propagator->updateSatisfiedConstraints();
}

cudaGlobal void Wrappers::initializeBitvectors(IntDomainsRepresentations* representations)
{
    representations->initializeBitvectors();
}
#endif
