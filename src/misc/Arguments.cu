#include <cstdlib>

#include <misc/Arguments.h>

using namespace std;

void Arguments::initialize()
{
    solverVersion = "Version 1.0";
    solutionsCount = 1;
    flatzincFile = nullptr;
}

int Arguments::parseArguments(int argc, char * argv[])
{
    argp_program_version = solverVersion;

    struct argp_option options[] =
        {
            { "all-solutions", 'a', 0, 0, "Print all solutions." },
            { "num-solutions", 'n', "Number", 0, "An upper bound on the number of solutions to output" },
            { 0 }
        };

    struct argp argp = { options, parse_opt, "FILE", 0 };

    return argp_parse(&argp, argc, argv, 0, 0, this);
}

int parse_opt(int key, char *arg, struct argp_state *state)
{
    Arguments* args = static_cast<Arguments*>(state->input);

    switch (key)
    {
        case 'a':
            args->solutionsCount = UINT_MAX;
            break;
        case 'n':
            args->solutionsCount = stoi(arg);
            if (args->solutionsCount == 0)
            {
                argp_failure (state, EXIT_FAILURE, 0, "Invalid number of solutions");
            }
            break;
        case ARGP_KEY_ARG:
            if (arg != nullptr)
            {
                int argLength = strlen(arg);
                if (argLength > 4)
                {
                    if (strcmp(arg + (argLength - 4), ".fzn") == 0)
                    {
                        args->flatzincFile = static_cast<char*>(malloc((argLength + 1) * sizeof(char)));
                        strcpy(args->flatzincFile, arg);
                    }
                }
            }
            break;
        case ARGP_KEY_END:
            if (args->flatzincFile == nullptr)
            {
                argp_error(state, "Flatzinc input file missing");
            }
            break;
    }

    return EXIT_SUCCESS;
}
