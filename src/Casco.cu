#include <cstdlib>
#include <iostream>

#include <misc/Utils.h>
#include <misc/CudaUtils.h>
#include <flatzinc/flatzinc.h>
#include <choosers/IntVariablesChooser.h>
#include <choosers/IntValuesChooser.h>
#include <searchers/IntBacktrackSearcher.h>
#include <misc/Arguments.h>
#include <misc/Wrappers.h>

#define HEAP_SIZE 512 * 1024 * 1024

using namespace std;

void printSolution(FlatZinc::Printer& printer, FlatZinc::FlatZincModel* fzModel);

int main(int argc, char * argv[])
{
    Arguments args;
    args.initialize();
    args.parseArguments(argc, argv);

    FlatZinc::Printer printer;
    FlatZinc::FlatZincModel* fzModel = FlatZinc::parse(args.flatzincFile, printer);
#ifdef CUDA
    LogUtils::cudaAssert(__PRETTY_FUNCTION__, cudaDeviceSetLimit(cudaLimitMallocHeapSize, HEAP_SIZE));

    int variablesBlockCount = KernelUtils::getBlockCount(fzModel->iv->count, DEFAULT_BLOCK_SIZE, false);
    Wrappers::initializeBitvectors<<<variablesBlockCount, DEFAULT_BLOCK_SIZE>>>(&fzModel->iv->domains.representations);
    LogUtils::cudaAssert(__PRETTY_FUNCTION__, cudaDeviceSynchronize());
#else
    fzModel->iv->domains.representations.initializeBitvectors();
#endif

    IntBacktrackSearcher* backtrackSearcher;
    MemUtils::malloc(&backtrackSearcher);
    backtrackSearcher->initialize(fzModel->iv, fzModel->intConstraints);
    backtrackSearcher->setVariablesChooserType(IntVariablesChooser::Type::InOrder);
    backtrackSearcher->setValuesChooserType(IntValuesChooser::Type::InOrder);

    bool* solutionFound;
    MemUtils::malloc(&solutionFound);
    *solutionFound = false;

#ifdef CUDA
    Wrappers::getFirstSolution<<<1, 1>>>(backtrackSearcher, solutionFound);
    LogUtils::cudaAssert(__PRETTY_FUNCTION__, cudaDeviceSynchronize());
#else
    backtrackSearcher->getFirstSolution(solutionFound);
#endif
    if (*solutionFound)
    {
        printSolution(printer, fzModel);
        args.solutionsCount -= 1;

        while (*solutionFound and args.solutionsCount > 0)
        {

#ifdef CUDA
            Wrappers::getNextSolution<<<1, 1>>>(backtrackSearcher, solutionFound);
            LogUtils::cudaAssert(__PRETTY_FUNCTION__, cudaDeviceSynchronize());
#else
            backtrackSearcher->getNextSolution(solutionFound);
#endif
            if (*solutionFound)
            {
                printSolution(printer, fzModel);
                args.solutionsCount -= 1;
            }
        }
        cout << "==========" << endl;
    }
    else
    {
        cout << "=====UNSATISFIABLE=====" << endl;
    }

    return EXIT_SUCCESS;
}

void printSolution(FlatZinc::Printer& printer, FlatZinc::FlatZincModel* fzModel)
{
    printer.print(cout, *fzModel);
    cout << "----------" << endl;
}
