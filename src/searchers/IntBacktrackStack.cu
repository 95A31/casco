#include <searchers/IntBacktrackStack.h>
#include <misc/CudaUtils.h>

void IntBacktrackStack::initialize(IntDomainsRepresentations* representations)
{
    this->representations = representations;

    stack.initialize(representations->bitvectors.size);
    stack.resize(representations->bitvectors.size);
    for (int i = 0; i < stack.size; i += 1)
    {
        stack[i].initialize(VECTOR_INITIAL_CAPACITY);
    }

    stackInfo.initialize(representations->bitvectors.size);
    stackInfo.resize(representations->bitvectors.size);
    for (int i = 0; i < representations->bitvectors.size; i += 1)
    {
        stackInfo[i].initialize();
    }
}

void IntBacktrackStack::deinitialize()
{
    for (int i = 0; i < stack.size; i += 1)
    {
        stack[i].deinitialize();
    }
    stack.deinitialize();

    for (int i = 0; i < stackInfo.size; i += 1)
    {
        stackInfo[i].deinitialize();
    }
    stackInfo.deinitialize();
}

cudaDevice void IntBacktrackStack::saveState(int backtrackLevel)
{
#ifdef CUDA
    int i = KernelUtils::getTaskIndex(blockDim.x * blockIdx.x + threadIdx.x);
    if (i >= 0 and i < stack.size)
#else
    for (int i = 0; i < stack.size; i += 1)
#endif
    {
        if (backtrackLevel == 0 || isDomainChanged(backtrackLevel - 1, i))
        {
            int min = representations->minimums[i];
            int max = representations->maximums[i];
            int offset = representations->offsets[i];
            int cardinality = representations->cardinalities[i];
            Vector<unsigned int>* bitvector = &representations->bitvectors[i];
            stack[i].pushRepresentation(min, max, offset, cardinality, bitvector);

            stackInfo[i].push_back(backtrackLevel);
        }
    }
}

cudaDevice void IntBacktrackStack::restoreState(int backtrackLevel)
{
#ifdef CUDA
    int i = KernelUtils::getTaskIndex(blockDim.x * blockIdx.x + threadIdx.x);
    if (i >= 0 and i < stack.size)
#else
    for (int i = 0; i < stack.size; i += 1)
#endif
    {
        if (isDomainChanged(backtrackLevel, i))
        {
            representations->minimums[i] = stack[i].minimums.back();
            representations->maximums[i] = stack[i].maximums.back();
            representations->offsets[i] = stack[i].offsets.back();
            representations->cardinalities[i] = stack[i].cardinalities.back();
            representations->bitvectors[i].copy(&stack[i].bitvectors.back());

        }
    }
}

cudaDevice void IntBacktrackStack::clearState(int backtrackLevel)
{
#ifdef CUDA
    int i = KernelUtils::getTaskIndex(blockDim.x * blockIdx.x + threadIdx.x);
    if (i >= 0 and i < stack.size)
#else
    for (int i = 0; i < stack.size; i += 1)
#endif
    {
        if (stackInfo[i].back() == backtrackLevel)
        {
            stack[i].popRepresentation();
            stackInfo[i].pop_back();
        }
    }
}

cudaDevice bool IntBacktrackStack::isDomainChanged(int backtrackLevel, int variable)
{
    int stackLevel = stackInfo[variable].size - 1;
    for (; stackLevel > 0; stackLevel -= 1)
    {
        if (backtrackLevel >= stackInfo[variable][stackLevel])
        {
            break;
        }
    }

    return stack[variable].cardinalities[stackLevel] != representations->cardinalities[variable];
}
