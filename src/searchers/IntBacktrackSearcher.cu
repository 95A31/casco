#include <searchers/IntBacktrackSearcher.h>
#include <misc/Utils.h>
#include <misc/Wrappers.h>
#include <misc/CudaUtils.h>

void IntBacktrackSearcher::initialize(IntVariables* variables, IntConstraints* constraints)
{
    this->variables = variables;
    this->constraints = constraints;

    state.initialize(variables->count);
    state.resize(variables->count);

    values.initialize(variables->count);
    values.resize(variables->count);

    stack.initialize(&variables->domains.representations);

    propagator.initialize(variables, constraints);
}

void IntBacktrackSearcher::deinitialize()
{
    state.deinitialize();

    values.deinitialize();

    stack.deinitialize();

    propagator.deinitialize();
}

void IntBacktrackSearcher::setVariablesChooserType(int variableChooserType)
{
    variablesChooser.initialzie(variableChooserType, variables, &state);
}

void IntBacktrackSearcher::setValuesChooserType(int valueChooserType)
{
    valuesChooser.initialzie(valueChooserType, variables);
}

cudaDevice void IntBacktrackSearcher::getFirstSolution(bool* solutionFound)
{
    //Preprocessing
    bool successfulPropagation = propagator.propagateConstraints();

    if (successfulPropagation)
    {
        level = 0;
        action = SetVariable;
        getNextSolution(solutionFound);
    }
    else
    {
        *solutionFound = false;
    }
}

cudaDevice void IntBacktrackSearcher::getNextSolution(bool* solutionFound)
{
#ifdef CUDA
    int variablesBlockCountDivergence = KernelUtils::getBlockCount(variables->count, DEFAULT_BLOCK_SIZE);
#endif

    while (level >= 0)
    {
        switch (action)
        {
            case SetVariable:
            {
#ifdef CUDA
                Wrappers::saveState<<<variablesBlockCountDivergence, DEFAULT_BLOCK_SIZE>>>(&stack, level);
                cudaDeviceSynchronize();
#else
                stack.saveState(level);
#endif

                if (variablesChooser.getVariable(level, &state[level]))
                {
                    action = SetFirstValue;
                }
                else
                {
                    LogUtils::error(__PRETTY_FUNCTION__, "Failed to set variable");
                }
            }
                break;

            case SetFirstValue:
            {
                if (not variables->isGround(state[level]))
                {
                    if (valuesChooser.getFirstValue(state[level], &values[level]))
                    {
                        variables->setGround(state[level], values[level]);
                        action = PropagateConstraints;
                    }
                    else
                    {
                        LogUtils::error(__PRETTY_FUNCTION__, "Failed to set first value");
                    }
                }
                else
                {
                    values[level] = variables->domains.representations.minimums[state[level]];

                    if (level < variables->count - 1)
                    {
                        level += 1;
                        action = SetVariable;
                    }
                    else
                    {
                        action = SetNextValue;

                        if (propagator.verifyConstraints())
                        {
                            *solutionFound = true;
                            return;
                        }
                    }
                }
            }
                break;

            case SetNextValue:
            {
#ifdef CUDA
                Wrappers::restoreState<<<variablesBlockCountDivergence, DEFAULT_BLOCK_SIZE>>>(&stack, level);
                cudaDeviceSynchronize();
#else
                stack.restoreState(level);
#endif
                if (valuesChooser.getNextValue(state[level], values[level], &values[level]))
                {
                    variables->setGround(state[level], values[level]);
                    action = PropagateConstraints;
                }
                else
                {
#ifdef CUDA
                    Wrappers::clearState<<<variablesBlockCountDivergence, DEFAULT_BLOCK_SIZE>>>(&stack, level);
                    cudaDeviceSynchronize();
#else
                    stack.clearState(level);
#endif
                    level = level - 1;
                    action = SetNextValue;
                }
            }
                break;
            case PropagateConstraints:
            {

                bool successfulPropagation = propagator.propagateConstraints();

                if (successfulPropagation)
                {
                    if (level < variables->count - 1)
                    {
                        level += 1;
                        action = SetVariable;
                    }
                    else
                    {
                        action = SetNextValue;

                        if (propagator.verifyConstraints())
                        {
                            *solutionFound = true;
                            return;
                        }
                    }
                }
                else
                {
                    action = SetNextValue;
                }
                break;
            }
        }
    }

    *solutionFound = false;
}
