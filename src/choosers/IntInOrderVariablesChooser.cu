#include <choosers/IntInOrderVariablesChooser.h>

#pragma diag_suppress set_but_not_used

cudaDevice bool IntInOrderVariablesChooser::getVariable(IntVariablesChooser* variablesChooser, int backtrackLevel, int* variable)
{
    *variable = backtrackLevel;
    return true;
}
