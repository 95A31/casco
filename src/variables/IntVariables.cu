#include <variables/IntVariables.h>

void IntVariables::initialize(int count)
{
   this->count = 0;

   domains.initialize(count);
   constraints.initialize(count);

}

void IntVariables::deinitialize()
{
    domains.deinitialize();

    for(int i = 0; i < constraints.size; i += 1)
    {
        constraints[i].deinitialize();
    }
    constraints.deinitialize();
}

void IntVariables::pushVariable(int min, int max)
{
    domains.pushDomain(min, max);

    constraints.resize_by_one();
    constraints.back().initialize();

    count += 1;
}

cudaDevice void IntVariables::setGround(int index, int value)
{
    assert(domains.representations.contain(index, value));

    domains.keepOnly(index, value);
}
